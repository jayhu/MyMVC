package com.freedom.web.mymvc.utils;

import io.netty.buffer.ByteBuf;

public class ByteBufUtils {
	public static String getContent(ByteBuf buf) {
		byte[] bytes = null;
		int length = 0;
		if (buf.hasArray()) {// 支持数组方式
			bytes = buf.array();
			length = bytes.length;
		} else {// 不支持数组方式
			length = buf.readableBytes();
			bytes = new byte[length];
			buf.getBytes(0, bytes);
		}
		//
		return new String(bytes);
	}
}
